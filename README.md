#########################################
#					#
#	Quaker for iOS			#
#	Written by Richard Lee		#
#	January 2015			#
#					#
#########################################

This application was written and created for the ANZ
iOS mobile developer role application.

The app contains the following Pods (third-party 'plugins')...
- MBProgressHUD
- AFNetworking
- SLCoreDataStack

Features include
- HTTPClient
- CoreDataStack ready to be used for syncing different NSManagedObjectContexts if neccessary
- FetchedResultsController
- CoreData model
- UIViewControllerAnimatedTransitioning
- Custom UITableViewCell with customisable action button
- Several options for data sorting


