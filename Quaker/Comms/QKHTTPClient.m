//
//  QKHTTPClient.m
//  Quaker
//
//  Created by Richard Lee on 3/02/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "QKHTTPClient.h"
#import "NSMutableURLRequest+RLHelper.h"
#import "AFNetworkReachabilityManager.h"

NSString* const ClientAPIBaseUrlKey = @"http://www.seismi.org/api/eqs/";

@interface QKHTTPClient()

@property (nonatomic,assign,getter = isOffline) BOOL offline;

@property (nonatomic,strong) NSString *secret;
@property (nonatomic,strong) NSMutableDictionary *settings;

- (AFHTTPRequestOperation *)performRequestWithMethod:(NSString *)method endpoint:(NSString *)endpoint params:(NSDictionary *)params completionBlock:(QKHTTPClientCompletionBlock)completionBlock;

- (void)logMessage:(NSString *)message;
- (void)logURLRequest:(NSURLRequest *)URLRequest;
- (NSString *)defaultsKeyForString:(NSString *)string;

@end


@implementation QKHTTPClient

#pragma mark - Initialisation

- (id)initWithBaseURL:(NSURL *)baseURL secret:(NSString *)secret settings:(NSDictionary *)settings
{
    if (self = [super initWithBaseURL:baseURL]) {
        
        // store the secret so we can sign requests
        NSParameterAssert(secret);
        self.secret = secret;
        
        self.responseSerializer.acceptableContentTypes = [self.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
        
        // set up the client to send and accept JSON requests by default
        //        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        //        [self setDefaultHeader:@"Accept" value:@"application/json"];
        
        // merge passed settings with defaults
        NSDictionary *defaultSettings = @{
                                          @"logging_enabled": @NO,
                                          };
        NSMutableDictionary *mergedSettings = [defaultSettings mutableCopy];
        if (settings)
        {
            [settings enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                mergedSettings[key] = obj;
            }];
        }
        self.settings = mergedSettings;
        
        // try to load a userToken from the user defaults
        NSLog(@"%@",[self defaultsKeyForString:@"api_user_token"]);
        _userToken = [[NSUserDefaults standardUserDefaults] objectForKey:[self defaultsKeyForString:@"api_user_token"]];
        
        // output log message
        [self logMessage:[NSString stringWithFormat:@"%@ initialised with settings: %@", NSStringFromClass([self class]), self.settings]];
        
    }
    return self;
}


+ (QKHTTPClient *)sharedClient {
    
    static dispatch_once_t once;
    static QKHTTPClient *sharedClient;
    dispatch_once(&once, ^{
        //        NSDictionary *settings = @{ @"logging_enabled" : @NO };
        //        sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:kAppApiBaseUrl] secret:@"htPdBaAErXqNhQnBwiOeEfKeQR4e0tortjpCtzll" settings:settings];
        sharedClient = [[QKHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:ClientAPIBaseUrlKey] secret:@"9J7fhY64uBs7y39HHye83nU3723f9fuy98H" settings:nil];
    });
    return sharedClient;
}


- (AFHTTPRequestOperation *)doGet:(NSString *)endpoint params:(NSDictionary *)params completionBlock:(QKHTTPClientCompletionBlock)completionBlock {
    
    return [self performRequestWithMethod:@"GET" endpoint:endpoint params:params completionBlock:completionBlock];
}


- (AFHTTPRequestOperation *)doPost:(NSString *)endpoint params:(NSDictionary *)params completionBlock:(QKHTTPClientCompletionBlock)completionBlock {
    
    return [self performRequestWithMethod:@"POST" endpoint:endpoint params:params completionBlock:completionBlock];
}


- (AFHTTPRequestOperation *)performRequestWithMethod:(NSString *)method endpoint:(NSString *)endpoint params:(NSDictionary *)params completionBlock:(QKHTTPClientCompletionBlock)completionBlock
{
    
    // compose and sign the URL request
    NSMutableURLRequest *req = [self.requestSerializer requestWithMethod:method URLString:[[NSURL URLWithString:endpoint relativeToURL:self.baseURL] absoluteString] parameters:params error:nil];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:req];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    
    self.responseSerializer.acceptableContentTypes = [self.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
    
    __weak QKHTTPClient *weakSelf = self;
    __weak AFHTTPRequestOperation *weakOp = operation;
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (weakSelf) {
            [weakSelf didFinishRequestOperation:weakOp withError:nil];
        }
        completionBlock(responseObject, operation, nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (weakSelf) {
            [weakSelf didFinishRequestOperation:weakOp withError:error];
        }
        completionBlock(nil, operation, error);
    }];
    
    // enqueue the operation
    [self logURLRequest:req];
    [self willStartRequestOperation:operation];
    [self.operationQueue addOperation:operation];
    
    // return the operation
    return operation;
    
}



#pragma mark - Cache Management Methods

- (QKIncrementalHTTPClientCacheStatus)cacheStatusForRequest:(NSURLRequest *)request
{
    
    // get the local cache timestamp for the requested URL
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *localCacheKey = [NSString stringWithFormat:@"lc_%@", [request.URL absoluteString]];
    int localCacheTimestamp = (int)[defaults integerForKey:[self defaultsKeyForString:localCacheKey]];
    
    // return the appropriate value
    if ([[NSDate date] timeIntervalSince1970] < localCacheTimestamp) return QKIncrementalHTTPClientValidCacheAvailable;
    if (localCacheTimestamp > 0) return QKIncrementalHTTPClientStaleCacheAvailable;
    return QKIncrementalHTTPClientNoCacheAvailable;
    
}


//- (void)updateCacheStatusForRequest:(NSURLRequest *)request response:(NSHTTPURLResponse *)response data:(id)responseData
//{
//    
//    // get a reference to the user defaults
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    // extract relevant headers from the response
//    int maxAge = [response cacheControlHeaderMaxAge];
//    int serverTimestamp = [response dateHeaderTimestamp];
//    
//    // set local cache timestamp
//    int localCacheTimestamp = [[NSDate date] timeIntervalSince1970] + maxAge;
//    NSString *localCacheKey = [NSString stringWithFormat:@"lc_%@", [request.URL absoluteString]];
//    [defaults setInteger:localCacheTimestamp forKey:[self defaultsKeyForString:localCacheKey]];
//    
//    // set remote cache timestamp if possible
//    if (serverTimestamp > 0) {
//        NSString *remoteCacheKey = [NSString stringWithFormat:@"rc_%@", [request.URL absoluteString]];
//        [defaults setInteger:serverTimestamp forKey:[self defaultsKeyForString:remoteCacheKey]];
//    }
//    
//    // persist new cache values
//    [defaults synchronize];
//    
//}


#pragma mark - AFHTTPClient Methods

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters
{
    NSMutableDictionary *modifiedParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
//    if (self.userToken != nil) modifiedParameters[@"api_user_token"] = self.userToken;
//    
//    
//    NSNumber *isRetina = [NSNumber numberWithBool:YES];
//    NSString *timestamp = [NSString stringWithFormat:@"%.0f",[[NSDate date] timeIntervalSince1970]];
//    NSString *nonce = [NSString generateUUID];
//    NSString *appSecret = @"9J7fhY64uBs7y39HHye83nU3723f9fuy98H";
//    NSString *apiCertificateId = @"nqUoYLhgxPVULqlQuYlOr5iFc686UOLcvUe3Mq2U9xITRnHRffxVJz4gKPiN7o2e";
//    NSString *apiVersion = @"2";
//    NSString *platform = @"ios";
//    NSString *osVersion = [NSString stringWithFormat:@"%.1f", CURRENT_OS()];
//    
//    NSString *deviceType;
//    if (IS_IPHONE_4) {
//        deviceType = @"4";
//    }
//    else if (IS_IPHONE_5) {
//        deviceType = @"5";
//    }
//    else if (IS_IPHONE_6) {
//        deviceType = @"6";
//    }
//    else if (IS_IPHONE_6_PLUS) {
//        deviceType = @"6+";
//    }
//    else {
//        deviceType = @"4";
//    }
    
    // If it is a call to the products endpoint then a different secret is used
//    NSString *request_id = [self stringToSha256:[appSecret stringByAppendingFormat:@"%@%@",nonce,timestamp]];
//    
//    modifiedParameters[@"api_certificate_id"] = apiCertificateId;
//    modifiedParameters[@"timestamp"] = timestamp;
//    modifiedParameters[@"nonce"] = nonce;
//    modifiedParameters[@"request_id"] = request_id;
//    modifiedParameters[@"retina_graphics"] = [NSString stringWithFormat:@"%d", isRetina.intValue];
//    modifiedParameters[@"api_version"] = apiVersion;
//    modifiedParameters[@"platform"] = platform;
//    modifiedParameters[@"platform_version"] = osVersion;
//    modifiedParameters[@"device"] = deviceType;
    
    
    NSURL *url = [NSURL URLWithString:path relativeToURL:self.baseURL];
    
    NSError *error = nil;
    //    NSString *queryString = [AFQueryStringFromParametersWithEncoding](parameters, self.requestSerializer.stringEncoding)];
    //    [[url absoluteString] stringByAppendingFormat:[path rangeOfString:@"?"].location == NSNotFound ? @"?%@" : @"&%@", queryString]
    return [self.requestSerializer requestWithMethod:method URLString:[url absoluteString] parameters:modifiedParameters error:&error];
}


#pragma mark - Overridable Methods

- (void)willStartRequestOperation:(AFHTTPRequestOperation *)operation
{
    NSLog(@"starting");
}

- (void)didFinishRequestOperation:(AFHTTPRequestOperation *)operation withError:(NSError *)error
{
    
}


#pragma mark - Empty Methods

- (void)willEnterOfflineMode
{
    
}

- (void)didExitOfflineMode
{
    
}


#pragma mark - General Purpose Helper Methods

- (void)logMessage:(NSString *)message
{
    if ([self.settings[@"logging_enabled"] boolValue]) NSLog(@"%@",message);
}

- (void)logURLRequest:(NSURLRequest *)URLRequest
{
    [self logMessage:[NSString stringWithFormat:@"%@ %@", URLRequest.HTTPMethod, [URLRequest.URL absoluteString]]];
}

- (NSString *)defaultsKeyForString:(NSString *)string
{
    NSParameterAssert(string);
    return [NSString stringWithFormat:@"%@_%@", NSStringFromClass([self class]), string];
}




@end
