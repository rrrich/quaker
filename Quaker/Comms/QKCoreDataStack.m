//
//  QKCoreDataStack.m
//  Quaker
//
//  Created by Richard Lee on 19/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "QKCoreDataStack.h"

@implementation QKCoreDataStack

- (NSString *)managedObjectModelName
{
    return @"Quaker";
}

@end
