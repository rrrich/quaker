//
//  QKHTTPClient.h
//  Quaker
//
//  Created by Richard Lee on 3/02/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

extern NSString* const ClientAPIBaseUrlKey;

typedef BOOL (^QKHTTPClientCompletionBlock)(NSDictionary *responseData, AFHTTPRequestOperation *operation, NSError *error);
typedef void (^QKHTTPClientLocalCompletionBlock)(BOOL staleData, NSError *error);

// cache status enum
typedef enum {
    QKIncrementalHTTPClientValidCacheAvailable,	// data is cached and within the expiration time specified by the server
    QKIncrementalHTTPClientStaleCacheAvailable,	// data is cached but past the expiration time specified by the server
    QKIncrementalHTTPClientNoCacheAvailable,	// no cached data available
} QKIncrementalHTTPClientCacheStatus;



@interface QKHTTPClient : AFHTTPRequestOperationManager

@property (nonatomic,strong) NSString *userToken;

+ (QKHTTPClient *)sharedClient;

// request methods
- (AFHTTPRequestOperation *)doGet:(NSString *)endpoint params:(NSDictionary *)params completionBlock:(QKHTTPClientCompletionBlock)completionBlock;
- (AFHTTPRequestOperation *)doPost:(NSString *)endpoint params:(NSDictionary *)params completionBlock:(QKHTTPClientCompletionBlock)completionBlock;


@end
