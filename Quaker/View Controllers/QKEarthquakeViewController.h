//
//  QKEarthquakeViewController.h
//  Quaker
//
//  Created by Richard Lee on 24/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "QKViewController.h"
#import "Earthquake+Quaker.h"

@class QKEarthquakeViewController;

@protocol QKEarthquakeViewControllerDelegate<NSObject>

- (void)earthquakeViewControllerDidFinish:(QKEarthquakeViewController *)exampleViewController;

@end


@interface QKEarthquakeViewController : QKViewController

@property (nonatomic, strong) Earthquake *earthquake;
@property (nonatomic, weak) id<QKEarthquakeViewControllerDelegate> delegate;

@property (nonatomic, strong) IBOutlet UILabel *timedateLabel;
@property (nonatomic, strong) IBOutlet UILabel *depthLabel;
@property (nonatomic, strong) IBOutlet UILabel *magnitudeLabel;
@property (nonatomic, strong) IBOutlet UILabel *regionLabel;

@end
