//
//  QKQuakesMainViewController.m
//  Quaker
//
//  Created by Richard Lee on 18/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "QKQuakesMainViewController.h"
#import "Earthquake+Quaker.h"
#import "QKTableViewCell.h"
#import "QKEarthquakeViewController.h"
#import "QKPresentAnimationController.h"
#import "MBProgressHUD.h"

@interface QKQuakesMainViewController ()<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate, QKEarthquakeViewControllerDelegate>

@property (nonatomic, strong) NSArray *earthquakes;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIView *modalView;

@property (nonatomic, strong) NSDateFormatter *timeDateFormatter;

@end

@implementation QKQuakesMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Earthquakes";
    
    
    // Setup FetchedResultsController properties
    self.entityName = @"Earthquake";
    self.predicate = nil;
    self.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"timedate" ascending:NO] ];
    self.cacheName = @"quakes";
    self.sectionNameKeyPath = nil;
    
    
    // Style and hide the modalView
    self.modalView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
    self.modalView.hidden = YES;
    
    
    // tableView settings - rowHeight and separators
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 80.0f;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    
    // Add a UIBarButtonItem to the navigation bar - this will control our 'sorting'
    // of the earthquake objects
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sortButton setTitle:@"Sort" forState:UIControlStateNormal];
    [sortButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f]];
    [sortButton setBackgroundColor:[UIColor clearColor]];
    [sortButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [sortButton setTitleColor:[UIColor orangeColor] forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(displaySortMenu:) forControlEvents:UIControlEventTouchUpInside];
    [sortButton setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 44.0f)];
    
    UIBarButtonItem *sortButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
    self.navigationItem.rightBarButtonItem = sortButtonItem;
    
    
    // NSDateFormatter for displaying timedate property nicely
    self.timeDateFormatter = [[NSDateFormatter alloc] init];
    self.timeDateFormatter.dateFormat = @"dd MMM yyyy hh:mm:ss aa";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSArray *)earthquakes {

    if (!_earthquakes) {
        _earthquakes = NSArray.new;
    }
    return _earthquakes;
}


- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    if ([self isFirstView]) {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [[QKHTTPClient sharedClient] doGet:@"eqs" params:@{} completionBlock:^BOOL(NSDictionary *responseData, AFHTTPRequestOperation *operation, NSError *error) {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if (error) {
                NSLog(@"ERROR: %@", error.description);
                return NO;
            }
            
            
            // Iterate through the JSON objects containing the data
            // for each respective earthquake, manipulate the dictionary of
            // values to suit the CoreData Earthquake entity and then create/update the object.
            for (NSDictionary *quakeData in responseData[@"earthquakes"]) {
            
                NSLog(@"creating:%@", quakeData[@"eqid"]);
                NSMutableDictionary *mutableData = quakeData.mutableCopy;
                NSString *depthString = quakeData[@"depth"];
                mutableData[@"depth"] = [NSNumber numberWithFloat:depthString.floatValue];
                mutableData[@"magnitude"] = [NSNumber numberWithFloat:[mutableData[@"magnitude"] floatValue]];
                mutableData[@"latitude"] = [NSNumber numberWithDouble:[mutableData[@"latitude"] doubleValue]];
                mutableData[@"longitude"] = [NSNumber numberWithDouble:[mutableData[@"longitude"] doubleValue]];
                mutableData[@"id"] = mutableData[@"eqid"];
                
                [Earthquake createOrUpdateWithData:mutableData inContext:[NSManagedObjectContext mainContext]];
            }
            
            NSError *saveError = nil;
            [[NSManagedObjectContext mainContext] save:&saveError];
            
            return YES;
        }];
    }
}


#pragma QKEarthquakeViewController methods

- (void)earthquakeViewControllerDidFinish:(QKEarthquakeViewController *)exampleViewController {
    [self dismissViewControllerAnimated:YES completion:^{
        self.modalView.hidden = YES;
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }];
}


#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"QKTableViewCell";
    QKTableViewCell *cell = (QKTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        // Load the top-level objects from the custom cell XIB.
        cell = [[[NSBundle mainBundle] loadNibNamed:@"QKTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self fetchedResultsController].fetchedObjects.count;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    Earthquake *earthquake = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self presentEarthquake:earthquake];
}


#pragma UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {

    NSArray *descriptors;
    
    if (buttonIndex == 0) {
    
        descriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"timedate" ascending:NO] ];
    }
    
    else if (buttonIndex == 1) {
    
        descriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"magnitude" ascending:NO] ];
    }
    
    else if (buttonIndex == 2) {
    
        descriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"depth" ascending:NO] ];
    }
    
    else {
    
        descriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"timedate" ascending:NO] ];
    }
    
    
    self.sortDescriptors = descriptors;
    [self performNewFetchRequest];
    [self.tableView reloadData];
}



#pragma UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source
{
    return [[QKPresentAnimationController alloc] init];
}


- (void)presentEarthquake:(Earthquake *)earthquake {

    QKEarthquakeViewController *earthquakeViewController = [[QKEarthquakeViewController alloc] initWithNibName:@"QKEarthquakeViewController" bundle:nil];
    earthquakeViewController.earthquake = earthquake;
    earthquakeViewController.delegate = self;
    earthquakeViewController.modalPresentationStyle = UIModalPresentationCustom;
    earthquakeViewController.transitioningDelegate = self;
    
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self presentViewController:earthquakeViewController animated:YES completion:^{
        self.modalView.hidden = NO;
    }];
}


- (void)showQuakeOverlay:(id)sender {

    UIButton *btn = (UIButton *)sender;
    NSInteger indexRow = btn.tag;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexRow inSection:0];
    
    Earthquake *earthquake = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self presentEarthquake:earthquake];
}

- (void)displaySortMenu:(id)sender {

    UIActionSheet *sortActionSheet = [[UIActionSheet alloc] initWithTitle:@"Sort by..." delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Most recent", @"Magnitude", @"Depth", nil];
    [sortActionSheet showInView:self.view];
}

- (void)configureCell:(QKTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {

    Earthquake *quake = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *timeDateString = [self.timeDateFormatter stringFromDate:quake.timedate];
    
    [cell setText:timeDateString subLabelText:[NSString stringWithFormat:@"Depth: %1.f", quake.depth.floatValue] thirdLabelText:[NSString stringWithFormat:@"Magnitude: %1.f", quake.magnitude.floatValue]];
    [cell setDidTapRightButtonBlock:^(id sender) {
        
        [self presentEarthquake:quake];
    }];
}



@end
