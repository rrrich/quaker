//
//  QKEarthquakeViewController.m
//  Quaker
//
//  Created by Richard Lee on 24/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "QKEarthquakeViewController.h"
#import <MapKit/MapKit.h>
#import "RLMapAnnotation.h"

@interface QKEarthquakeViewController ()<MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

//
@end

@implementation QKEarthquakeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.clipsToBounds = NO;
    
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    self.view.layer.shadowOpacity = 0.5;
    self.view.layer.shadowRadius = 10.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {

    [super viewDidLayoutSubviews];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.view.bounds];
    self.view.layer.shadowPath = shadowPath.CGPath;
}


- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    if ([self isFirstView]) {
        
        NSDateFormatter *timeDateFormatter = [[NSDateFormatter alloc] init];
        timeDateFormatter.dateFormat = @"dd MMM yyyy hh:mm:ss aa";
        
        self.timedateLabel.text = [timeDateFormatter stringFromDate:self.earthquake.timedate];
        self.depthLabel.text = [NSString stringWithFormat:@"Depth: %.1f", self.earthquake.depth.floatValue];
        self.magnitudeLabel.text = [NSString stringWithFormat:@"Magnitude: %.1f", self.earthquake.magnitude.floatValue];
        self.regionLabel.text = self.earthquake.region;
        
        [self setupMapView];
    }
}


- (void)viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];
    
    // Create annotation and add to the map
    CLLocationCoordinate2D quakeLocation;
    quakeLocation.latitude = self.earthquake.latitude.doubleValue;
    quakeLocation.longitude = self.earthquake.longitude.doubleValue;
    
    RLMapAnnotation *annotation = [[RLMapAnnotation alloc] initWithTitle:self.earthquake.timedate.description andCoordinate:quakeLocation];
    [self.mapView addAnnotation:annotation];
}


#pragma MKMapViewDelegate methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    // creates regular green pin annotation view
    MKPinAnnotationView *pinView = nil;
    if(annotation != mapView.userLocation)
    {
        static NSString *defaultPinID = @"FWStoreViewController";
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        
        if ( pinView == nil )
            pinView = [[MKPinAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        
        pinView.pinColor = MKPinAnnotationColorRed;
        pinView.animatesDrop = YES;
        pinView.canShowCallout = YES;
        
        //        pinView.canShowCallout = YES;
        //        pinView.enabled = YES;
        //
        //        UIImage *pinImage = [UIImage imageNamed:@"tickpin.png"];
        //        pinView.image = pinImage;
        //        pinView.centerOffset = CGPointMake(2.0f, (-pinImage.size.height / 2)+4.0f);
        //        pinView.calloutOffset = CGPointMake(-6.0f, -2.0f);
    }
    else {
        return nil;
    }
    
    pinView.annotation = annotation;
    
    return pinView;
}


#pragma mark Custom Methods

- (void)setupMapView {
    
    self.mapView.mapType = MKMapTypeStandard;
    
    CLLocationCoordinate2D centerLocation;
    centerLocation.latitude = self.earthquake.latitude.doubleValue;
    centerLocation.longitude = self.earthquake.longitude.doubleValue;
    
    //Set initial UIState for MapView
    CLLocationCoordinate2D zoomLocation = centerLocation;
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 500, 500);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    [self.mapView setRegion:adjustedRegion animated:NO];
}

- (IBAction)closeView:(id)sender {

    if (self.delegate && [self.delegate respondsToSelector:@selector(earthquakeViewControllerDidFinish:)]) {
        [self.delegate earthquakeViewControllerDidFinish:self];
    }
}


@end
