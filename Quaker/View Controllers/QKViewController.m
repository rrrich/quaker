//
//  QKViewController.m
//  Quaker
//
//  Created by Richard Lee on 18/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "QKViewController.h"

@interface QKViewController ()

@property (nonatomic, strong) NSNumber *firstView;

@end

@implementation QKViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.firstView = @1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    // We've now viewed this screen...
    self.firstView = @0;
}

- (BOOL)isFirstView {
    
    return self.firstView.boolValue;
}


@end
