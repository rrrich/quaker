//
//  QKQuakesMainViewController.h
//  Quaker
//
//  Created by Richard Lee on 18/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "QKFetchedResultsViewController.h"

@interface QKQuakesMainViewController : QKFetchedResultsViewController

@end
