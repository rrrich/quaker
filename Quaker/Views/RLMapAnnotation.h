//
//  RLMapAnnotation.h
//  Quaker
//
//  Created by Richard Lee on 1/02/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface RLMapAnnotation : NSObject <MKAnnotation>

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;

- (id)initWithTitle:(NSString *)ttl andCoordinate:(CLLocationCoordinate2D)c2d;


@end
