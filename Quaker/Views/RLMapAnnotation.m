//
//  RLMapAnnotation.m
//  Quaker
//
//  Created by Richard Lee on 1/02/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "RLMapAnnotation.h"

@implementation RLMapAnnotation

- (id)initWithTitle:(NSString *)ttl andCoordinate:(CLLocationCoordinate2D)c2d {
    
    self = [super init];
    
    _title = ttl;
    _coordinate = c2d;
    return self;
}

@end
