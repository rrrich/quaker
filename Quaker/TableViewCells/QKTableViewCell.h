//
//  QKTableViewCell.h
//  Quaker
//
//  Created by Richard Lee on 24/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QKTableViewCell : UITableViewCell


// Set the text for the main
- (void)setText:(NSString *)text subLabelText:(NSString *)subLabelText thirdLabelText:(NSString *)thirdLabelText;

// Assign a block to the rightButton property of the cell
- (void)setDidTapRightButtonBlock:(void (^)(id sender))didTapButtonBlock;

// rightButton tag
- (void)setRightButtonTag:(NSInteger)buttonTag;


@end
