//
//  QKTableViewCell.m
//  Quaker
//
//  Created by Richard Lee on 24/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "QKTableViewCell.h"

@interface QKTableViewCell()

@property (copy, nonatomic) void (^didTapRightButtonBlock)(id sender);

@property (strong, nonatomic) IBOutlet UILabel *mainLabel;
@property (strong, nonatomic) IBOutlet UILabel *subLabel;
@property (strong, nonatomic) IBOutlet UILabel *thirdLabel;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;


@end

@implementation QKTableViewCell

- (void)awakeFromNib {
    
    // Initialization code
    self.mainLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f];
    self.subLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    self.subLabel.textColor = [UIColor blueColor];
    
    self.thirdLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    self.thirdLabel.textColor = [UIColor redColor];
    
    // rightButton
    [self.rightButton addTarget:self action:@selector(didTapRightButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setText:(NSString *)text subLabelText:(NSString *)subLabelText thirdLabelText:(NSString *)thirdLabelText {

    self.mainLabel.text = text;
    self.subLabel.text = subLabelText;
    self.thirdLabel.text = thirdLabelText;
}

- (void)setRightButtonTag:(NSInteger)buttonTag {

    self.rightButton.tag = buttonTag;
}

- (void)didTapRightButton:(id)sender {
    if (self.didTapRightButtonBlock) {
        self.didTapRightButtonBlock(sender);
    }
}


@end
