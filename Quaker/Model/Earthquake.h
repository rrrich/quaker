//
//  Earthquake.h
//  Quaker
//
//  Created by Richard Lee on 19/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Earthquake : NSManagedObject

@property (nonatomic, retain) NSString * remoteId;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSDate * timedate;
@property (nonatomic, retain) NSNumber * magnitude;
@property (nonatomic, retain) NSNumber * depth;
@property (nonatomic, retain) NSString * region;
@property (nonatomic, retain) NSString * src;

@end
