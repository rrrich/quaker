//
//  Earthquake.m
//  Quaker
//
//  Created by Richard Lee on 19/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "Earthquake.h"


@implementation Earthquake

@dynamic remoteId;
@dynamic latitude;
@dynamic longitude;
@dynamic timedate;
@dynamic magnitude;
@dynamic depth;
@dynamic region;
@dynamic src;

@end
