//
//  Earthquake+Quaker.h
//  Quaker
//
//  Created by Richard Lee on 21/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "Earthquake.h"

@interface Earthquake (Quaker)

+ (NSArray *)getEarthquakesInContext:(NSManagedObjectContext *)context sortDescriptors:(NSArray *)sortDescriptors;

@end
