//
//  Earthquake+Quaker.m
//  Quaker
//
//  Created by Richard Lee on 21/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "Earthquake+Quaker.h"

@implementation Earthquake (Quaker)

- (NSDictionary *)customMappings
{
    return @{ @"lat": @"latitude", @"lon": @"longitude" };
}

+ (NSArray *)getEarthquakesInContext:(NSManagedObjectContext *)context sortDescriptors:(NSArray *)sortDescriptors {

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:@"Earthquake" inManagedObjectContext:context];
    fetchRequest.predicate = nil;
    fetchRequest.sortDescriptors = sortDescriptors;
    
    NSError *error = nil;
    return [context executeFetchRequest:fetchRequest error:&error];
}

@end
