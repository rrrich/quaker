//
//  QKFetchedResultsViewController.h
//  Quaker
//
//  Created by Richard Lee on 1/02/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "QKViewController.h"

@interface QKFetchedResultsViewController : QKViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSString *entityName;
@property (nonatomic, strong) NSPredicate *predicate;
@property (nonatomic, strong) NSArray *sortDescriptors;
@property (nonatomic, strong) NSString *sectionNameKeyPath;
@property (nonatomic, strong) NSString *cacheName;

- (void)performNewFetchRequest;

@end
