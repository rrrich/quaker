//
//  NSManagedObjectContext+QKCoreDataStack.m
//  Quaker
//
//  Created by Richard Lee on 19/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "NSManagedObjectContext+QKCoreDataStack.h"

@implementation NSManagedObjectContext (QKCoreDataStack)

+ (NSManagedObjectContext *)backgroundContext
{
    return [[QKCoreDataStack sharedInstance] backgroundThreadManagedObjectContext];
}

+ (NSManagedObjectContext *)mainContext
{
    return [[QKCoreDataStack sharedInstance] mainThreadManagedObjectContext];
}

@end
