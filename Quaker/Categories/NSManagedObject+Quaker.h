//
//  NSManagedObject+Quaker.h
//  Growing Together
//
//  Created by Wellcom on 10/11/2014.
//  Copyright (c) 2014 Wellcom. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (GrowingTogether)

// Delete methods
+ (void)delete:(id)record_id fromContext:(NSManagedObjectContext *)context;
+ (void)deleteAllFromContext:(NSManagedObjectContext *)context;

+ (id)get:(id)record_id fromContext:(NSManagedObjectContext *)context;
+ (id)getOrCreateNew:(id)record_id fromContext:(NSManagedObjectContext *)context;

+ (id)createOrUpdateWithData:(NSDictionary *)data inContext:(NSManagedObjectContext *)context;
+ (id)createOrUpdateWithData:(NSDictionary *)data objectId:(id)objectId inContext:(NSManagedObjectContext *)context;

@end
