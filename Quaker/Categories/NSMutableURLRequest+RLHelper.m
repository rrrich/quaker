//
//  RLHelper.m
//  Quaker
//
//  Created by Richard Lee on 3/02/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import "NSMutableURLRequest+RLHelper.h"

@implementation NSMutableURLRequest (RLHelper)


- (void)signWithSecret:(NSString *)secret
{
    NSParameterAssert(secret);
    //    NSString *nonce = [[[NSUUID UUID] UUIDString] lowercaseString];
    //    NSString *signature = [nonce SHA256HMACWithKey:secret];
    //    NSString *authorizationHeader = [NSString stringWithFormat:@"DREAMWALK-TOKEN-V1 nonce=\"%@\", signature=\"%@\"", nonce, signature];
    //    [self setValue:authorizationHeader forHTTPHeaderField:@"Authorization"];
}


#pragma mark -

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method
                                      path:(NSString *)path
                                parameters:(NSDictionary *)parameters
{
    NSParameterAssert(method);
    
    if (!path) {
        path = @"";
    }
    
    NSURL *url = [NSURL URLWithString:path relativeToURL:[NSURL URLWithString:ClientAPIBaseUrlKey]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:method];
    //    [request setAllHTTPHeaderFields:self.defaultHeaders];
    
    if (parameters) {
        if ([method isEqualToString:@"GET"] || [method isEqualToString:@"HEAD"] || [method isEqualToString:@"DELETE"]) {
            //            url = [NSURL URLWithString:[[url absoluteString] stringByAppendingFormat:[path rangeOfString:@"?"].location == NSNotFound ? @"?%@" : @"&%@", AFQueryStringFromParametersWithEncoding(parameters, NSUTF8StringEncoding)]];
            
            url = [NSURL URLWithString:[url absoluteString]];
            [request setURL:url];
        } else {
            //            NSString *charset = (__bridge NSString *)CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
            //            NSError *error = nil;
            
            //            switch (self.parameterEncoding) {
            //                case AFFormURLParameterEncoding:;
            //                    [request setValue:[NSString stringWithFormat:@"application/x-www-form-urlencoded; charset=%@", charset] forHTTPHeaderField:@"Content-Type"];
            //                    [request setHTTPBody:[AFQueryStringFromParametersWithEncoding(parameters, NSUTF8StringEncoding) dataUsingEncoding:NSUTF8StringEncoding]];
            //                    break;
            //                case AFJSONParameterEncoding:;
            //                    [request setValue:[NSString stringWithFormat:@"application/json; charset=%@", charset] forHTTPHeaderField:@"Content-Type"];
            //                    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:parameters options:(NSJSONWritingOptions)0 error:&error]];
            //                    break;
            //                case AFPropertyListParameterEncoding:;
            //                    [request setValue:[NSString stringWithFormat:@"application/x-plist; charset=%@", charset] forHTTPHeaderField:@"Content-Type"];
            //                    [request setHTTPBody:[NSPropertyListSerialization dataWithPropertyList:parameters format:NSPropertyListXMLFormat_v1_0 options:0 error:&error]];
            //                    break;
            //            }
            
            //            if (error) {
            //                NSLog(@"%@ %@: %@", [self class], NSStringFromSelector(_cmd), error);
            //            }
        }
    }
    
    return request;
}



@end
