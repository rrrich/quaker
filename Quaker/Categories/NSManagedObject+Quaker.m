//
//  NSManagedObject+Quaker.m
//  Growing Together
//
//  Created by Wellcom on 10/11/2014.
//  Copyright (c) 2014 Wellcom. All rights reserved.
//

#import "NSManagedObject+Quaker.h"

@implementation NSManagedObject (GrowingTogether)


+ (NSString *)entityName
{
    return NSStringFromClass([self class]);
}

+ (NSString *)primaryKeyName
{
    return @"remoteId";
}

- (NSDictionary *)customMappings
{
    return [NSDictionary dictionary];
}

- (NSDate *)dateFromString:(NSString *)string
{
    static dispatch_once_t onceToken;
    static NSDateFormatter *dateFormatter;
    
    // @"2014-11-30 23:59:59";
    
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        
        //        2014-11-30 23:59:59
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        //        dateFormatter.dateFormat = @"yyyy-MM-dd HH:MM:SS";
        //        [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    });
    return [dateFormatter dateFromString:string];
}


#pragma Delete methods

+ (void)delete:(id)record_id fromContext:(NSManagedObjectContext *)context
{
    NSManagedObject *record = [self get:record_id fromContext:context];
    if (record) {
        [context deleteObject:record];
    }
}

+ (void)deleteAllFromContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fr = [[NSFetchRequest alloc] init];
    fr.entity = [NSEntityDescription entityForName:[self entityName] inManagedObjectContext:context];
    fr.includesPropertyValues = NO;
    NSArray *records = [context executeFetchRequest:fr error:nil];
    for (NSManagedObject *record in records) {
        [context deleteObject:record];
    }
}



#pragma mark - Query Helpers

+ (id)get:(id)record_id fromContext:(NSManagedObjectContext *)context
{
    NSString *key = [self primaryKeyName];
    NSString *entityName = [self entityName];
    NSManagedObject *record;
    NSError *error;
    NSFetchRequest *fr = [[NSFetchRequest alloc] init];
    fr.entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    fr.predicate = [NSPredicate predicateWithFormat:@"self.%@ == %@", key, record_id];
    NSArray *fetchedObjects = [context executeFetchRequest:fr error:&error];
    if ([fetchedObjects count] > 0) {
        record = [fetchedObjects objectAtIndex:0];
    } else {
        record = nil;
    }
    return record;
}

+ (id)getOrCreateNew:(id)record_id fromContext:(NSManagedObjectContext *)context
{
    NSString *key = [self primaryKeyName];
    NSString *entityName = [self entityName];
    NSManagedObject *record;
    record = [self get:record_id fromContext:context];
    if (record == nil) {
        record = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
        [record setValue:record_id forKey:key];
    }
    return record;
}


#pragma mark - Data Mapping Methods

+ (id)createOrUpdateWithData:(NSDictionary *)data inContext:(NSManagedObjectContext *)context
{
    NSManagedObject *object = [self getOrCreateNew:data[@"id"] fromContext:context];
    [object updateWithData:data];
    return object;
}

+ (id)createOrUpdateWithData:(NSDictionary *)data objectId:(id)objectId inContext:(NSManagedObjectContext *)context
{
    NSManagedObject *object = [self getOrCreateNew:objectId fromContext:context];
    [object updateWithData:data];
    return object;
}


- (void)updateWithData:(NSDictionary *)data
{
    NSDictionary *mappings = [self customMappings];
    NSDictionary *attributes = [[self entity] attributesByName];
    NSDictionary *relationships = [[self entity] relationshipsByName];
    [data enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        // check if we have a custom mapping for the key; if not, try converting from snake_case to camelCase
        NSString *mappedKey = mappings[key];
        if (!mappedKey) {
            mappedKey = [self camelCase:key];
        }
        
        // try to find an attribute matching the mapped key
        NSAttributeDescription *attributeDescription = attributes[mappedKey];
        if (attributeDescription) {
            if ([attributeDescription attributeType] == NSDateAttributeType) {
                [self setValue:[self dateFromString:obj] forKey:mappedKey];
            }
            else if ([attributeDescription attributeType] == NSDoubleAttributeType) {
                [self setValue:[NSNumber numberWithDouble:[obj doubleValue]] forKey:mappedKey];
            }
            else if ([attributeDescription attributeType] == NSInteger64AttributeType) {
                [self setValue:[NSNumber numberWithInt:[obj intValue]] forKey:mappedKey];
            }
            else {
                
                if (obj != (id)[NSNull null])
                    [self setValue:obj forKey:mappedKey];
            }
            return;
        }
        
        // try finding a relationship matching the mapped key
        NSRelationshipDescription *relationshipDescription = relationships[mappedKey];
        if (relationshipDescription) {
            if ([relationshipDescription isToMany]) {
                
                // to-many relationship
                if ([obj isKindOfClass:[NSArray class]]) {
                    NSMutableSet *relatedObjects = [self mutableSetValueForKey:mappedKey];
                    for (NSDictionary *relatedData in obj) {
                        NSManagedObject *relatedObject = [NSClassFromString([[relationshipDescription destinationEntity] managedObjectClassName]) createOrUpdateWithData:relatedData inContext:self.managedObjectContext];
                        [relatedObjects addObject:relatedObject];
                    }
                }
                    
            } else {
                
                // to-one relationship
                NSManagedObject *relatedObject = [NSClassFromString([[relationshipDescription destinationEntity] managedObjectClassName]) createOrUpdateWithData:obj inContext:self.managedObjectContext];
                [self setValue:relatedObject forKey:mappedKey];
                
            }
        }
        
    }];
}


#pragma mark - General Purpose Helpers

- (NSString *)camelCase:(NSString *)string
{
    NSString *result = [[[string stringByReplacingOccurrencesOfString:@"_" withString:@" "] capitalizedString] stringByReplacingOccurrencesOfString:@" " withString:@""];
    return [result stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[result substringWithRange:NSMakeRange(0, 1)] lowercaseString]];
}



@end
