//
//  NSManagedObjectContext+QKCoreDataStack.h
//  Quaker
//
//  Created by Richard Lee on 19/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (QKCoreDataStack)

+ (NSManagedObjectContext *)backgroundContext;
+ (NSManagedObjectContext *)mainContext;

@end
