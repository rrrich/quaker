//
//  RLHelper.h
//  Quaker
//
//  Created by Richard Lee on 3/02/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableURLRequest (RLHelper)

- (void)signWithSecret:(NSString *)secret;

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method
                                      path:(NSString *)path
                                parameters:(NSDictionary *)parameters;

@end
