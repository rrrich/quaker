//
//  QKPresentAnimationController.h
//  Quaker
//
//  Created by Richard Lee on 31/01/2015.
//  Copyright (c) 2015 Richard Lee. All rights reserved.
//

@import UIKit;

@interface QKPresentAnimationController : NSObject<UIViewControllerAnimatedTransitioning>

@end
